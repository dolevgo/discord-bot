﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Discord_Bot
{
    class CommandHandler
    {
        private readonly DiscordSocketClient _client;
        private readonly CommandService _cmdService;
        private readonly IServiceProvider _services;
        private readonly string _prefix;

        public CommandHandler(DiscordSocketClient client, CommandService cmdService, IServiceProvider services, string prefix = "$")
        {
            _client = client;
            _cmdService = cmdService;
            _services = services;
            _prefix = prefix;
        }

        public async Task InitializeAsync()
        {
            await _cmdService.AddModulesAsync(Assembly.GetEntryAssembly(), _services);
            _cmdService.Log += Log;
            _client.MessageReceived += Client_MessageRecieved;
        }

        private Task Log(LogMessage arg)
        {
            Console.WriteLine(arg.Message);
            return Task.CompletedTask;
        }

        private async Task Client_MessageRecieved(SocketMessage socketMessage)
        {
            if (socketMessage.Author.IsBot) return;

            SocketUserMessage message = socketMessage as SocketUserMessage;

            if (message is null) return;

            int argpos = 0;

            if (message.Channel is SocketDMChannel)
            {
                //private message
                Console.WriteLine($"{message.CreatedAt} message: '{message.Content}' from '{message.Author.Username}' in private chat.");
                await message.Channel.SendMessageAsync($"Shut the fuck up.");
            }
            else if (message.HasStringPrefix(_prefix, ref argpos) || message.HasMentionPrefix(_client.CurrentUser, ref argpos))//does the message start with "!"?
            {
                SocketCommandContext context = new SocketCommandContext(_client, message);
                Console.WriteLine($"{message.CreatedAt} message: '{message.Content}' from '{message.Author.Username}' in server '{context.Guild}', channel '{message.Channel}'.");
                IResult result = await _cmdService.ExecuteAsync(context, argpos, _services);
                if (!result.IsSuccess)
                {
                    Console.WriteLine("Error- " + result.ErrorReason);
                }
            }
        }

    }
}
