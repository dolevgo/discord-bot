﻿using System.Threading.Tasks;
using Discord.Commands;
using Discord;

namespace Discord_Bot.Modules
{
    public class ServerManagement : ModuleBase<SocketCommandContext>
    {

        
        [Command("kick"), RequireOwner, RequireUserPermission(GuildPermission.KickMembers)]
        public async Task AsyncKick(IGuildUser user, [Remainder] string reason = "You suck")
        {
            await user.KickAsync(reason);
            await user.SendMessageAsync($"You were kicked for- '{reason}'");
            await ReplyAsync($"Kicked {user.Username}!");
        }

        [Command("ban"), RequireOwner, RequireUserPermission(GuildPermission.BanMembers)]
        public async Task AsyncBan(IGuildUser user, [Remainder] string reason = "You suck")
        {
            await AsyncBan(user, reason);
            await user.SendMessageAsync($"You were banned for- '{reason}'");
            await ReplyAsync($"Banned {user.Username}!");
        }



    }
}
