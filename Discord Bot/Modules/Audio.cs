﻿using System.Threading.Tasks;
using Discord.Commands;
using Discord;
using Discord.Audio;
using Victoria;
using Discord_Bot.Services;
using Discord.WebSocket;
using System.Security;
using Victoria.Entities;

namespace Discord_Bot.Modules
{
    public class Audio : ModuleBase<ICommandContext>
    {
        private MusicService _musicService;

        public Audio(MusicService musicService)
        {
            _musicService = musicService;
        }

        [Command("join")]
        public async Task JoinVoiceServer()
        {
            SocketGuildUser user = Context.User as SocketGuildUser;
            if (user.VoiceChannel is null)
            {
                ReplyAsync("Nice voice channel lol");
                return;
            }
            ITextChannel textChannel = Context.Channel as ITextChannel;

            _musicService.ConnectAsync(user.VoiceChannel, textChannel);
            await ReplyAsync($"Joined voice channel {user.VoiceChannel.Name} and bound to text channel {textChannel.Mention}");
        }

        [Command("leave")]
        public async Task LeaveVoiceServer()
        {
            var user = Context.User as SocketGuildUser;
            if (user.VoiceChannel is null)
            {
                await ReplyAsync("Please join the channel the bot is in to make it leave.");
            }
            else
            {
                await _musicService.LeaveAsync(user.VoiceChannel);
                await ReplyAsync($"Bot has now left {user.VoiceChannel.Name}");
            }
        }

        [Command("play")]
        public async Task Play([Remainder]string query)
        {
            var user = Context.User as SocketGuildUser;
            EmbedBuilder response = await _musicService.PlayAsync(query, Context.Guild.Id, user.VoiceChannel, Context.Channel as ITextChannel);
            await ReplyAsync(embed: buildFormat(response, Context.Client.CurrentUser).Build());
        }

        [Command("stop")]
        public async Task Stop()
        {
            EmbedBuilder response =  await _musicService.StopAsync(Context.Guild.Id);
            await ReplyAsync(embed: buildFormat(response, Context.Client.CurrentUser).Build());
        }

        [Command("skip")]
        public async Task Skip()
        {
            EmbedBuilder response = await _musicService.SkipAsync(Context.Guild.Id);

            await ReplyAsync(embed: buildFormat(response, Context.Client.CurrentUser).Build());
        }

        [Command("np")]
        public async Task CurrentlyPlaying()
        {
            EmbedBuilder response = await _musicService.CurrentlyPLayingAsync(Context.Guild.Id);

            await ReplyAsync(embed: buildFormat(response, Context.Client.CurrentUser).Build());
        }

        [Command("queue")]
        public async Task Queue()
        {
            EmbedBuilder response = await _musicService.GetQueueAsync(Context.Guild.Id);

            await ReplyAsync(embed: buildFormat(response, Context.Client.CurrentUser).Build());
        }

        [Command("lyrics")]
        public async Task Lyrics()
        {
            EmbedBuilder response = await _musicService.GetLyrics(Context.Guild.Id, Context.Channel);

            await ReplyAsync(embed: buildFormat(response, Context.Client.CurrentUser).Build());
        }

        [Command("reset")]
        public async Task Reset()
        {
            EmbedBuilder response = await _musicService.ResetTrack(Context.Guild.Id);

            await ReplyAsync(embed: buildFormat(response, Context.Client.CurrentUser).Build());
        }

        [Command("volume")]
        public async Task SetVolume(int volume)
        {
            EmbedBuilder response = await _musicService.SetVolume(volume, Context.Guild.Id);

            await ReplyAsync(embed: buildFormat(response, Context.Client.CurrentUser).Build());
        }

        private EmbedBuilder buildFormat(EmbedBuilder previous, ISelfUser currentUser)
        {
            return previous
                .WithCurrentTimestamp()
                .WithColor(Color.Green)
                .WithAuthor(currentUser);
        }

    }
}
