﻿using System.Threading.Tasks;
using Discord.Commands;
using Discord;

namespace Discord_Bot.Modules
{
    [Group("create")]
    public class Create : ServerManagement
    {
        [Command]
        public async Task AsyncCreateDefault()
        {
            await ReplyAsync("Parameters are needed.");
        }

        [Command("text channel"), RequireUserPermission(GuildPermission.ManageChannels)]
        public async Task AsyncCreateTextChannel([Remainder] string name)
        {
            ITextChannel textChannel = await Context.Guild.CreateTextChannelAsync(name);
            await ReplyAsync($"Created the text channel {textChannel.Mention}.");
        }

        [Command("voice channel"), RequireUserPermission(GuildPermission.ManageChannels)]
        public async Task AsyncCreateVoiceChannel([Remainder] string name)
        {
            IVoiceChannel voiceChannel = await Context.Guild.CreateVoiceChannelAsync(name);
            await ReplyAsync($"Created the voice channel {voiceChannel.Name}.");
        }
    }
}
