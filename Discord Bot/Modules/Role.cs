﻿using System.Threading.Tasks;
using Discord.Commands;
using Discord;
using Discord.WebSocket;
using System;
using System.Collections.Generic;

namespace Discord_Bot.Modules
{
    [Group("role"), RequireUserPermission(GuildPermission.ManageRoles)]
    public class Role : ServerManagement
    {
        [Command("give")]
        public async Task AsyncGiveRole(IGuildUser user, string roleString)
        {
            IRole role = null;
            if (!ContainsRole(Context.Guild.Roles, roleString, out role))
            {
                await ReplyAsync($"The role {roleString} does not exist.");
                return;
            }
            await user.AddRoleAsync(role);
            await ReplyAsync($"Gave {user.Mention} the role {role.Mention}.");
        }

        [Command("take")]
        public async Task AsyncTakeRole(SocketGuildUser user, string roleString)
        {
            IRole role = null;
            if (!ContainsRole(Context.Guild.Roles, roleString, out role))
            {
                await ReplyAsync($"The role {roleString} does not exist.");
                return;
            }
            if (!ContainsRole(user.Roles, roleString))
            {
                await ReplyAsync($"The user {user.Mention} does not have the role '{roleString}'.");
                return;
            }
            await user.RemoveRoleAsync(role);
            await ReplyAsync($"Took the role {role.Mention} from {user.Mention}.");

        }


        [Command("delete")]
        public async Task AsyncDelteRole(IRole role)
        {
            if (ContainsRole(Context.Guild.Roles, role.Name))
            {
                await role.DeleteAsync();
                await ReplyAsync($"Deleted the role {role.Name}.");
            }
            else
            {
                await ReplyAsync($"Role {role.Mention} does not exist.");
            }
        }

        [Command("delete")]
        public async Task AsyncDelteRole([Remainder] string roleString)
        {
            IRole role;
            if (ContainsRole(Context.Guild.Roles, roleString, out role))
            {
                await role.DeleteAsync();
                await ReplyAsync($"Deleted the role {role.Name}.");
            }
            else
            {
                await ReplyAsync($"Role {roleString} does not exist.");
            }
        }

        [Command("create")]
        public async Task AsyncCreateRole([Remainder] string name)
        {
            IRole role = await Context.Guild.CreateRoleAsync(name);
            await ReplyAsync($"Created the role {role.Mention}.");
        }

        private bool ContainsRole(IReadOnlyCollection<SocketRole> roles, string roleString, out IRole role)
        {
            role = null;
            foreach (IRole temprole in roles)
            {
                if (temprole.Name == roleString)
                {
                    role = temprole;
                    return true;
                }
            }
            return false;
        }

        private bool ContainsRole(IReadOnlyCollection<SocketRole> roles, string roleString)
        {
            foreach (IRole temprole in roles)
            {
                if (temprole.Name == roleString)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
