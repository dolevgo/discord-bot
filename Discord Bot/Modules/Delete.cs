﻿using System.Threading.Tasks;
using Discord.Commands;
using Discord;

namespace Discord_Bot.Modules
{
    [Group("delete")]
    public class Delete : ServerManagement
    {
        [Command]
        public async Task AsyncDeleteDefault()
        {
            await ReplyAsync("Parameters are needed.");
        }

        [Command("text channel"), RequireUserPermission(GuildPermission.ManageChannels)]
        public async Task AsyncDeleteTextChannel(ITextChannel channel)
        {
            try
            {
                await channel.DeleteAsync();
                await ReplyAsync($"Deleted the text channel {channel.Name}.");
            }
            catch
            {
                await ReplyAsync($"Couldn't delete the text channel {channel.Mention}.");
            }

        }

        [Command("voice channel"), RequireUserPermission(GuildPermission.ManageChannels)]
        public async Task AsyncDeleteVoiceChannel(IVoiceChannel channel)
        {
            try
            {
                await channel.DeleteAsync();
                await ReplyAsync($"Deleted the voice channel {channel.Name}.");
            }
            catch
            {
                await ReplyAsync($"Couldn't delete the voice channel {channel.Name}.");
            }
        }
    }
}
