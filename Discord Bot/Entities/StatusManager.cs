﻿using Discord;
using Discord.WebSocket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Discord_Bot.Entities
{
    class StatusManager
    {
        private DiscordSocketClient _client;
        private IEnumerable<string> _statuses;
        private int _statusCount;
        private int _statusIndex;
        private Timer _timer;

        public StatusManager(DiscordSocketClient client, IEnumerable<string> statuses)
        {
            _client = client;
            _statuses = statuses;
            _statusCount = _statuses.Count();
            _statusIndex = 0;
            _timer = new Timer(TimerUpdate, null, TimeSpan.Zero, TimeSpan.FromSeconds(5));
        }

        private async void TimerUpdate(Object stateInfo)
        {
            await _client.SetGameAsync(_statuses.ElementAtOrDefault(_statusIndex), type: ActivityType.Playing);
            _statusIndex = (_statusIndex + 1) % _statusCount;
        }
    }
}
