﻿using Discord;
using Discord.Audio;
using Discord.WebSocket;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Victoria;
using Victoria.Entities;
using Victoria.Queue;

namespace Discord_Bot.Services
{
    public class MusicService
    {
        private LavaRestClient _lavaRestClient;
        private LavaSocketClient _lavaSocketClient;
        private DiscordSocketClient _client;
        private List<ulong> connectedServers;

        public MusicService(LavaRestClient lavaRestClient, LavaSocketClient lavaSocketClient, DiscordSocketClient client)
        {
            _lavaRestClient = lavaRestClient;
            _lavaSocketClient = lavaSocketClient;
            _client = client;

            connectedServers = new List<ulong>();
        }

        public Task InitializeAsync()
        {
            _client.Ready += ClientReadyAsync;
            _lavaSocketClient.Log += LogAsync;
            _lavaSocketClient.OnTrackFinished += TrackFinished;
            return Task.CompletedTask;
        }

        public async Task<EmbedBuilder> SetVolume(int volume, ulong guildId)
        {
            EmbedBuilder builder = new EmbedBuilder();
            LavaPlayer player = _lavaSocketClient.GetPlayer(guildId);
            if (player is null || !player.IsPlaying)
                return builder.WithTitle("Nothing is playing!");

            await player.SetVolumeAsync(volume);
            return builder.WithTitle("Volume set").WithDescription($"Current volume: {player.CurrentVolume}");
        }

        private async Task TrackFinished(LavaPlayer player, LavaTrack track, TrackEndReason reason)
        {
            if (!reason.ShouldPlayNext())
                return;


            if (!player.Queue.TryDequeue(out IQueueObject item) || !(item is LavaTrack nextTrack))
            {
                await player.TextChannel.SendMessageAsync("Track dead lol");
                return;
            }

            await player.PlayAsync(nextTrack);
        }

        private Task LogAsync(LogMessage logMessage)
        {
            Console.WriteLine(logMessage.Message);
            return Task.CompletedTask;
        }

        private async Task ClientReadyAsync()
        {
            await _lavaSocketClient.StartAsync(_client, new Configuration()
            {
                LogSeverity = LogSeverity.Verbose
            });
        }

        public async Task<EmbedBuilder> SkipAsync(ulong guildId)
        {
            LavaPlayer _player = _lavaSocketClient.GetPlayer(guildId);
            EmbedBuilder builder = new EmbedBuilder();

            // if player is null or theres nothing playing
            if (_player is null || _player.Queue.Items.Count() == 0 && !_player.IsPlaying)
                return builder.WithTitle("Queue is empty!").WithDescription("Nothing to skip to");
            // if its the last song
            if (_player.Queue.Items.Count() == 0)
                return await StopAsync(guildId);

            LavaTrack oldTrack = await _player.SkipAsync();

            builder = builder
                .WithTitle($"Skipped {oldTrack.Title}")
                .WithDescription($"Next: {linkFormat(_player.CurrentTrack.Title, _player.CurrentTrack.Uri)}.")
                .WithThumbnailUrl(await _player.CurrentTrack.FetchThumbnailAsync());

            if (_player.Queue.Items.Count() == 0)
            {
                builder.Description += $"{Environment.NewLine}Queue is now empty!";
            }

            return builder;
        }

        public async Task<EmbedBuilder> GetQueueAsync(ulong guildId)
        {
            LavaPlayer _player = _lavaSocketClient.GetPlayer(guildId);

            EmbedBuilder builder = new EmbedBuilder
            {
                Title = "Queue"
            };

            if (_player is null || (!_player.IsPlaying && _player.Queue.Items.Count() == 0))
                return builder.WithDescription("The queue is empty!");

            List<LavaTrack> tracks = new List<LavaTrack>();

            if (_player.IsPlaying)
                tracks.Add(_player.CurrentTrack);

            foreach (IQueueObject queueObject in _player.Queue.Items)
                tracks.Add(queueObject as LavaTrack);

            string description = "";
            for (int i = 0; i < tracks.Count; i++)
            {
                description += $"{i + 1}. {linkFormat(tracks[i].Title, tracks[i].Uri)}: {tracks[i].Length}";
                description += Environment.NewLine;
            }

            return builder.WithDescription(description);
        }
        public async Task<EmbedBuilder> ResetTrack(ulong guildId)
        {
            LavaPlayer _player = _lavaSocketClient.GetPlayer(guildId);

            EmbedBuilder builder = new EmbedBuilder
            {
                Title = "Reset Track"
            };

            if (_player is null || !_player.IsPlaying)
                return builder.WithDescription("Nothing is playing!");

            _player.CurrentTrack.ResetPosition();

            return builder.WithDescription($"Reset {linkFormat(_player.CurrentTrack.Title, _player.CurrentTrack.Uri)}.");
        }

        public async Task<EmbedBuilder> GetLyrics(ulong guildId, IMessageChannel channel)
        {
            LavaPlayer _player = _lavaSocketClient.GetPlayer(guildId);

            EmbedBuilder builder = new EmbedBuilder
            {
                Title = "Lyrics"
            };

            if (_player is null || !_player.IsPlaying)
                return builder.WithDescription("Nothing is playing!");

            return builder.WithDescription((await _player.CurrentTrack.FetchLyricsAsync()).Substring(0, 2048));
        }

        public async Task<EmbedBuilder> CurrentlyPLayingAsync(ulong guildId)
        {
            LavaPlayer _player = _lavaSocketClient.GetPlayer(guildId);

            EmbedBuilder builder = new EmbedBuilder
            {
                Title = "Currently Playing"
            };

            if (_player is null || !_player.IsPlaying)
                return builder.WithDescription("Nothing is being playing right now.");

            LavaTrack currentTrack = _player.CurrentTrack;

            return builder
                .WithDescription($"{linkFormat(currentTrack.Title, currentTrack.Uri)} By {currentTrack.Author}{Environment.NewLine}" +
                    $"{currentTrack.Position}/{currentTrack.Length}")
                .WithThumbnailUrl(await currentTrack.FetchThumbnailAsync());

        }

        public async Task ConnectAsync(SocketVoiceChannel voiceChannel, ITextChannel textChannel)
        {
            if (!connectedServers.Contains(voiceChannel.Id))
            {
                connectedServers.Add(voiceChannel.Id);
            }
            await _lavaSocketClient.ConnectAsync(voiceChannel, textChannel);
        }

        public async Task LeaveAsync(SocketVoiceChannel voiceChannel)
        {
            if (connectedServers.Contains(voiceChannel.Id))
            {
                connectedServers.Remove(voiceChannel.Id);
            }
            await _lavaSocketClient.DisconnectAsync(voiceChannel);
        }

        public async Task<EmbedBuilder> PlayAsync(
            string query,
            ulong guildId,
            SocketVoiceChannel voiceChannel,
            ITextChannel textChannel)
        {
            EmbedBuilder builder = new EmbedBuilder();

            LavaPlayer _player = _lavaSocketClient.GetPlayer(guildId);

            if (!connectedServers.Contains(voiceChannel.Id) || _player is null)
            {
                await ConnectAsync(voiceChannel, textChannel);
                _player = _lavaSocketClient.GetPlayer(guildId);
            }

            SearchResult results = await _lavaRestClient.SearchYouTubeAsync(query);

            if (results.LoadType == LoadType.NoMatches || results.LoadType == LoadType.LoadFailed)
            {
                return builder.WithTitle($"Could not play {query}.");
            }

            LavaTrack track = results.Tracks.FirstOrDefault();

            if (_player.IsPlaying)
            {
                _player.Queue.Enqueue(track);
                builder = builder.WithTitle($"{linkFormat(track.Title, track.Uri)} added to queue.")
                    .WithDescription($"Position in queue: {indexOfTrack(_player, track) + 1}.");
            }
            else
            {
                await _player.PlayAsync(track);
                builder = builder.WithTitle($"Now playing {linkFormat(track.Title, track.Uri)}");
            }

            builder = builder.WithThumbnailUrl(await track.FetchThumbnailAsync());
            return builder;
        }

        private int indexOfTrack(LavaPlayer player, LavaTrack track)
        {
            int index = 0;
            foreach (LavaTrack item in player.Queue.Items)
            {
                if (item == track)
                    return index;
                index++;
            }

            return -1;
        }

        public async Task<EmbedBuilder> StopAsync(ulong guildId)
        {
            LavaPlayer _player = _lavaSocketClient.GetPlayer(guildId);
            EmbedBuilder builder = new EmbedBuilder();
            if (_player is null)
                return builder.WithTitle("Nothing is playing");

            _player.StopAsync();

            return builder.WithTitle("Music stopped.");
        }

        private string linkFormat(string text, Uri uri)
        {
            return $"[{text}]({uri})";
        }
    }
}
