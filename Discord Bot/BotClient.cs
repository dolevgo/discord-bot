﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Discord_Bot.Services;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Victoria;
using System.Threading;
using Discord_Bot.Entities;

namespace Discord_Bot
{
    public class BotClient
    {
        private string _botToken;
        private string _prefix;
        private DiscordSocketClient _client;
        private CommandService _cmdService;
        private IServiceProvider _services;
        StatusManager _statusManager;

        private List<string> _statuses = new List<string> { "end", "my", "pain", "please" };

        public BotClient(DiscordSocketClient client = null, CommandService service = null)
        {
            _client = client ?? new DiscordSocketClient(new DiscordSocketConfig
            {
                AlwaysDownloadUsers = true,
                MessageCacheSize = 50,
                LogLevel = LogSeverity.Debug
            });

            _cmdService = service ?? new CommandService(new CommandServiceConfig
            {
                LogLevel = LogSeverity.Verbose,
                CaseSensitiveCommands = false
            });

            _statusManager = new StatusManager(_client, _statuses);
        }

        public async Task InitializeAsync(string botToken, string prefix)
        {
            _prefix = prefix;
            _botToken = botToken;

            await _client.LoginAsync(TokenType.Bot, _botToken);// LOGIN BOT
            await _client.StartAsync();// START THE BOT

            _client.Log += Log; // FOR FEEDBACK
            _client.UserJoined += AnnounceUserJoin;
            _client.JoinedGuild += JoinedGuild;
            _client.LeftGuild += LeftGuild;
            _client.UserBanned += UserBanned;
            _client.UserLeft += UserLeft;

            _services = SetupServices();

            CommandHandler cmdHandler = new CommandHandler(_client, _cmdService, _services, _prefix);
            await cmdHandler.InitializeAsync();

            await _services.GetRequiredService<MusicService>().InitializeAsync();
            await Task.Delay(-1);
        }


        private IServiceProvider SetupServices()
            => new ServiceCollection()
            .AddSingleton(_client)
            .AddSingleton(_cmdService)
            .AddSingleton<LavaRestClient>()
            .AddSingleton<LavaSocketClient>()
            .AddSingleton<MusicService>()
            .BuildServiceProvider();

        private Task UserLeft(SocketGuildUser user)
        {
            user.SendMessageAsync("Goodbye old friend.");
            return Task.CompletedTask;
        }

        private Task UserBanned(SocketUser arg1, SocketGuild guild)
        {
            foreach (ITextChannel channel in guild.TextChannels)
                channel.SendMessageAsync($"{arg1.Username} is now behind bars!");
            return Task.CompletedTask;

        }

        private Task LeftGuild(SocketGuild guild)
        {
            foreach (ITextChannel channel in guild.TextChannels)
                channel.SendMessageAsync($"Cya later niggas");
            return Task.CompletedTask;
        }

        private Task JoinedGuild(SocketGuild guild)
        {
            foreach (ITextChannel channel in guild.TextChannels)
                channel.SendMessageAsync($"What's up niggas");
            return Task.CompletedTask;
        }

        private Task AnnounceUserJoin(SocketGuildUser user)
        {
            foreach (ITextChannel channel in user.Guild.TextChannels)
                channel.SendMessageAsync($"And another one- {user.Mention}");
            return Task.CompletedTask;
        }

        private Task Log(LogMessage arg)
        {
            Console.WriteLine(arg.Message);
            return Task.CompletedTask;
        }
    }
}
